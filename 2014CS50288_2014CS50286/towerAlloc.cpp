#include <iostream>
#include <fstream>
#include <malloc.h>
#include <string>
#include <string.h>
#include <cstdlib>
#include <algorithm>
#include <ctime>
#include<cstdio>

#define max 10000
using namespace std;

string inputfile="input.txt", outputfile="output.txt";

struct node // bid node structure
{   int bidid;
	int cid; //company
	double price;  
	int norc;  // no. of regions in one bid
	int region[max];
	int heuristic; //heuristic function
};

double tim; 
int nor;
int nob;
int noc;
struct node tob[max]; // total no. of bids
struct node tobsorted[max]; //sorted bids

bool com[max],reg[max]; //keeps record of which companies and regions can be selected in the remaining unprocessed bids
bool bid[max]; // final bids

bool localmaxbid[max];
double localmax = 0.0;

ofstream myfile;

bool wayToSort(node i, node j) {
	 return i.heuristic > j.heuristic;
}


//function to take input - read from console by redirection
void readFile()
{
    ifstream file(inputfile.c_str() );

	string g;
	string timf;
	string norf;
	string nobf;
	string nocf;
	
	getline(file,timf);
	getline(file,g);
	getline(file,norf);
	getline(file,g);
	getline(file,nobf);
	getline(file,g);
	getline(file,nocf);
	getline(file,g);
	
	tim = stod(timf);
	nor = stoi(norf);
	nob = stoi(nobf);
	noc = stoi(nocf);
	
	for(int i=0;i<nob;i++)
	{
		
		string ch;
		getline(file,ch);
		
		int t=0;int j=0;
		char ch1[max];
		while(ch[t]!=' ')
		{
			ch1[j]=ch[t];
			j++;t++;
		}
		
		ch1[j]='\0';
		tob[i].cid=atoi(ch1);
	
		ch1[0]='\0';j=0;t++;
		while(ch[t]!=' ')
		{
			ch1[j]=ch[t];
			j++;t++;
		}
		ch1[j]='\0';			
		tob[i].price=strtod (ch1, NULL);
		t++;
		
		int x=0;
		int w=t;		
		while(ch[t]!='#')
		{
			if(ch[t]==' ')
			{	x++;}
			t++;
		}
		tob[i].norc=x;
		tob[i].heuristic=tob[i].price/tob[i].norc; //heristic assigned

		t=w;
		for(int qq=0;qq<x;qq++)
		{
			ch1[0]='\0';j=0;
			while(ch[t]!=' ')
			{
				ch1[j]=ch[t];
				j++;t++;
			}
			t++;
			ch1[j]='\0';
			tob[i].region[qq]=atoi(ch1);
		}
		
		tob[i].bidid = i; //bid id assigned
		tobsorted[i]=tob[i];
		getline(file,g);	
	}
	
	sort(tobsorted, tobsorted + nob, wayToSort);
}

void fill(int);
bool checkReg(int);

void getRandom() 
{
	
	int num1,i;
	num1 = rand()%nob;
	int tossstart =rand()%4;
	double revenue = 0;  //total revenue

    int fstart = rand()%nob;
	int fend = rand()%nob;

	int bestval = 0;
	int beststart = -1;
	
	if(fstart>fend){
		int temp = fend;
		fend = fstart;
		fstart = fend;
	}
		
    for(int j=fstart;j<=fend;j++){
	    if(com[tob[tobsorted[j].bidid].cid] || checkReg(tobsorted[j].bidid)){
			continue;
		}
			
		if(tob[tobsorted[j].bidid].heuristic>bestval){
			beststart = tobsorted[j].bidid;
			bestval = tob[tobsorted[j].bidid].heuristic;
			break;
		}
    }
	
	
	if(tossstart<2 || beststart==-1){
		
		fill(num1);
	    revenue += tob[num1].price;
	}else{
		
			fill(beststart);
	        revenue += tob[beststart].price;

		
	}
	
	
	while(true){
		
		int bestneighbour=-1;
		int bestrandomnbr = -1;
		int bestrandomval = 0;
		int bestvalue=0;
		
		int fnbrs = rand()%nob;
	    int fnbre = rand()%nob;
	
	    if(fnbrs>fnbre){
		    int temp = fnbre;
		    fnbre = fnbrs;
		    fnbrs = temp;
	    }
		
		for(int j=fnbrs;j<=fnbre;j++){
			if(com[tob[tobsorted[j].bidid].cid] || checkReg(tobsorted[j].bidid)){
			    continue;
		    }
			
		    if(tob[tobsorted[j].bidid].heuristic>bestrandomval){
			    bestrandomnbr = tobsorted[j].bidid;
			    bestrandomval = tob[tobsorted[j].bidid].heuristic;
			    break;
		    }
		}
		
		for(i=0;i<nob;i++){
			if(com[tob[tobsorted[i].bidid].cid] || checkReg(tobsorted[i].bidid)){
			    continue;
		    }
			
		    if(tob[tobsorted[i].bidid].heuristic>bestvalue){
			    bestneighbour = tobsorted[i].bidid;
			    bestvalue = tob[tobsorted[i].bidid].heuristic;
			    break;
		    }
		}
		
		if(bestneighbour==-1){
			break;
		}

		int toss = rand()%4;
		if(toss<2 || bestrandomnbr==-1){
			fill(bestneighbour);
	        revenue += tob[bestneighbour].price;
		}else{
			
			fill(bestrandomnbr);
			revenue += tob[bestrandomnbr].price;
		}

	}
	
	

	if(localmax<revenue){
		localmax = revenue;
		//cout<<(int)localmax<<endl;
		myfile.open(outputfile.c_str());

		for(int i=0; i<nob; i++){
			localmaxbid[i] = bid[i];
			if(localmaxbid[i]){
				myfile<<i<<" ";
			}
		}
		myfile<<"#";
		myfile.close();
	}

	
	for(int i=0; i<max; i++){
		com[i] = false;
		reg[i] = false;
		bid[i] = false;
	}
	
}

//helper function of getRandom function
void fill(int bidno)
{
	com[tob[bidno].cid]=true;
	bid[bidno]=true;
	for(int i=0;i<tob[bidno].norc;i++)
	{
		reg[tob[bidno].region[i]]=true;
	}
}	

//helper function of getRandom function
bool checkReg(int bidno)
{
	for(int i=0;i<tob[bidno].norc;i++)
	{
		if(reg[tob[bidno].region[i]]==true)
			return true;
	}
	return false;
}


int main(int argc, char *argv[])
{
	clock_t start;
	double duration;
	start = clock();
	
	if(argc>=3){
		inputfile = argv[1];
		outputfile = argv[2];
	}
	srand(time(0));  //seed of random generator
	readFile();


	while(true){
		getRandom();
        duration = (clock() - start ) / (double) CLOCKS_PER_SEC;
        if(duration>tim*60*0.99){
			return 0;
		}
	}	
	
	return 0;
}

